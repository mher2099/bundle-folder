import * as $ from 'jquery'
import moment from 'moment'

const updateTime = () => {
    $('#time').text(moment().format())
}
setInterval(updateTime, 500)
